'use strict';

const gulp			= require('gulp'),
plumber				= require('gulp-plumber'),
rename				= require('gulp-rename'),
args				= require('yargs').argv,
gulpif				= require('gulp-if'),
notify				= require('gulp-notify'),
del					= require('del'),

jade				= require('gulp-jade-php'),

uglify				= require('gulp-uglify-es').default,

imagemin			= require('gulp-imagemin'),

sass				= require('gulp-sass')(require('node-sass')),
sourcemaps			= require('gulp-sourcemaps'),
compass				= require('compass-importer'),
assetFunctions		= require('node-sass-asset-functions'),
autoprefixer		= require('gulp-autoprefixer'),

browserSync			= require('browser-sync').create(),

isRelease			= args.release || false;

sass.compiler		= require('node-sass');


// CLEAN
gulp.task('clean', () => {
	return del([
		'../library/css',
		'../library/fonts',
		'../library/images',
		'../library/js',
		'../pages',
		'../layout',
		'../blocks',
		'../*.php',
		'!../functions.php',
	], {
		force	: true
	});
});

// JADE
gulp.task('jade', function buildHTML() {
	return gulp.src(['../assets/jade/**/*.jade', '!../assets/jade/**/_*.jade'])
		.pipe(plumber({
			errorHandler: notify.onError("Error: <%= error.message %>")
		}))
		.pipe(jade({
			'pretty': (!isRelease) ? true : false,
			'locals': {
				'echo': function(str) {
					return "<?php echo " + str + " ?>"
				},
				'image': function(src) {
					return ("<?php echo get_template_directory_uri() ?>/library/images/" + src);
				},
				'background': function(src, fromWP) {
					var url = "/library/images/";
					url += src;
					if (fromWP) {
						return ("background-image:url('<?php echo " + src + "?>')");
					} else {
						return ("background-image:url('<?php echo get_template_directory_uri() . \"" + url + "\" ?>')");
					}
				},
				'css': function(value) {
					return ("<?php echo get_template_directory_uri() ?>/library/css/" + value);
				},
				'js': function(value) {
					return ("<?php echo get_template_directory_uri() ?>/library/js/" + value);
				},
				'library': function(src) {
					return ("<?php echo get_template_directory_uri() ?>/library/" + src);
				}
			}
		}))
		.pipe(plumber.stop())
		.pipe(gulp.dest("../"))
});

// IMAGES
gulp.task('images', () => {
	
	if (!isRelease) console.warn("WARNING: running whitout --release, skipping image minification");
	
	return gulp.src([
		"../assets/images/**/*"
	])
	.pipe((plumber({
		errorHandler: notify.onError("Error: <%= error.message %>")
	})))
	.pipe(imagemin({
		optimizationLevel	: 3,
		progressive			: true,
		interlaced			: true
	}))
	.pipe(plumber.stop())
	.pipe(gulp.dest('../library/images/'));
});

// STYLES
gulp.task('styles', () => {
	return gulp.src([
		"../assets/sass/**/*.scss"
	])
	.pipe(gulpif(!isRelease, sourcemaps.init()))
	.pipe((plumber({
		errorHandler: notify.onError("Error: <%= error.message %>")
	})))
	.pipe(sass({
		importer		: compass,
		outputStyle		: isRelease? "compressed" : "nested",
		functions		: assetFunctions({
			images_path			: "../images/",
			http_images_path	: "../images/",
			fonts_path			: "../fonts/",
			http_fonts_path		: "../fonts/"
			}
		)
	}))
	.pipe(autoprefixer({
		'overrideBrowserslist' : [
			'last 2 versions'
		]
	}))
	.pipe(gulpif(!isRelease, sourcemaps.write('.')))
	.pipe(plumber.stop())
	.pipe(gulp.dest('../library/css/'))
});

// SCRIPTS
gulp.task('js', () => {
	return gulp.src([
		"../assets/js/**/*.js"
	])
	.pipe((plumber({
		errorHandler: notify.onError("Error: <%= error.message %>")
	})))
	.pipe(gulpif(isRelease, uglify()))
	.pipe(rename({
		suffix: '.min'
	}))
	.pipe(plumber.stop())
	.pipe(gulp.dest('../library/js/libs/'))
});

// JSON
gulp.task('json', () => {
	return gulp.src([
		"../assets/json/**/*.json"
	])
	.pipe((plumber({
		errorHandler: notify.onError("Error: <%= error.message %>")
	})))
	.pipe(gulpif(isRelease, uglify()))
	.pipe(plumber.stop())
	.pipe(gulp.dest('../library/json/'))
});

// FONTS
gulp.task('fonts', () => {
	return gulp.src([
		"../assets/fonts/**/*"
	])
	.pipe(gulp.dest('../library/fonts/'))
});

// BUILD
gulp.task('build',
	gulp.series(
		'clean',
		gulp.parallel(
			'images',
			'jade',
			'js',
			'styles',
			'fonts',
			'json'
		)
	)
);

gulp.task('watch', () => {
	browserSync.init({
        // server: {
        //     baseDir: "../"
        // },
		proxy: {
			target: "http://localhost/posto-corinthians",
		}
    });
	gulp.watch("../assets/jade/**/*.jade", gulp.series('jade')).on('change', browserSync.reload);
	gulp.watch("../assets/sass/**/*.scss", gulp.series('styles')).on('change', browserSync.reload);
	gulp.watch("../assets/js/**/*.js", gulp.series('js')).on('change', browserSync.reload);
	gulp.watch("../assets/json/**/*.json", gulp.series('json')).on('change', browserSync.reload);
	gulp.watch("../assets/fonts/**/*", gulp.series('fonts')).on('change', browserSync.reload);
	gulp.watch("../assets/images/**/*", gulp.series('images')).on('change', browserSync.reload);
});

// WATCH
gulp.task( 'default', gulp.series( 'build', (done) => done() ));
