<?php

/************************************
LOAD THEME CORE
************************************/
require_once( 'theme/theme.php' );
require_once( 'theme/gutenberg.php' );

/************************************
LOAD CUSTOM POST TYPE
************************************/
require_once( 'custom_posts/custom-post-type.php' );

/************************************
CUSTOM FUNCTIONS
************************************/
require_once('functions/custom-functions.php');

/************************************
CUSTOM HOOKS
************************************/
require_once( 'functions/custom-hooks.php' );

/************************************
AJAX
************************************/
require_once('functions/ajax.php');

/************************************
REST
************************************/
require_once('functions/rest.php');

/************************************
ACF
************************************/
require_once('functions/acf.php');

?>
