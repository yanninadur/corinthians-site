<?php

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', function () {
	flush_rewrite_rules();
} );


/*
	Replace Taxonomy slug with Post Type slug in url
*/
add_filter( 'generate_rewrite_rules', function ( $wp_rewrite ) {
    $rules = array();
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
    // get all custom post types
    $post_types = get_post_types(array(
        'public' => true,
        '_builtin' => false),
    'objects');

    foreach ($post_types as $post_type) {
    	$post_type_name = $post_type->name;
    	$post_type_slug = $post_type->name;
    	if ( $post_type->rewrite && $post_type->rewrite['slug'] ){
    		$post_type_slug = $post_type->rewrite['slug'];
    	}
        foreach ($taxonomies as $taxonomy)
        {
            // check if taxonomy is registered for this custom type
            if ($taxonomy->object_type[0] == $post_type_name) {

                // get all categories (terms)
                $categories = get_categories(array(
                    'type' => $post_type_name,
                    'taxonomy' => $taxonomy->name,
                    'hide_empty' => 0
                ));

                // make rules
                foreach ($categories as $category) {
                  $rules[$post_type_slug . '/' . $category->slug . '/?$'] =
                    'index.php?' . $category->taxonomy . '=' . $category->slug;
                }
            }
        }
    }

    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
} );

/*******************************
	CUSTOM POST TYPES
*******************************/
require_once('project.php');
?>
