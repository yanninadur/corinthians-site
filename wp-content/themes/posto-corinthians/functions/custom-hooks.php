<?php
	/*******************************
	STYLES & SCRIPTS
	********************************/
	// deferred style
	add_filter( 'style_loader_tag', function ( $tag, $handle ) {
		if ( strpos ( $handle, "inline-" ) === 0 ) {
			if ( !preg_match('/^<link.*?href=(["\'])(.*?)\1.*$/', $tag, $m) ) return $tag;
			ob_start();
			?>
				<style><?php echo file_get_contents( $m[2] ) ?></style>
			<?php
			return ob_end_flush();
		}
		
		if ( strpos ( $handle, "async-" ) === 0 ) {
			if ( !preg_match('/^<link.*?href=(["\'])(.*?)\1.*$/', $tag, $m) ) return $tag;
			ob_start();
			?>
				<link rel="preload" href="<?php echo $tag ?>" as="style" onload="this.onload=null;this.rel='stylesheet'">
				<noscript><link rel="stylesheet" href="<?php echo $tag ?>"></noscript>
			<?php
			return ob_end_flush();
		}
		
		return $tag;
	}, 10, 2);

	// deferred scripts
	add_filter( 'script_loader_tag', function ( $tag, $handle ) {

		// default async js
		if ( in_array( $handle, array(
			"wp-embed"
		) ) ) return str_replace( ' src', ' async src', $tag );

		// default defered js
		if ( in_array( $handle, array() ) ) return str_replace( ' src', ' defer src', $tag );

		if ( strpos ( $handle, "async-" ) === 0 ) return str_replace( ' src', ' async src', $tag );

		if ( strpos ( $handle, "defer-" ) === 0 ) return str_replace( ' src', ' defer src', $tag );

		return $tag;
	}, 10, 2);

	/*******************************
	TITLE
	********************************/
	add_filter( 'wp_title', function ( $title, $sep = "|" ) {
		if ( is_feed() )
			return $title;

		// vars
		global $page, $paged;		
		$site_description	= get_bloginfo( 'description', 'display' );
		$is_front			= ( isset( $site_description ) && ( is_home() || is_front_page() ) ) ? true : false;
		
			
		// Add the blog name
		if ( !$is_front ) $title .= $sep;
		$title .= get_bloginfo( 'name', 'display' );
	
		// Add the blog description for the home / front page.
		if ( $is_front )
			$title .= " {$sep} {$site_description}";
	
		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
			$title .= " {$sep} " . sprintf( __( 'Página %s', '_s' ), max( $paged, $page ) );
		
		return $title;

	} , 10, 2 );
?>
