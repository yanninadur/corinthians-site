<?php
	function ajax_example() {
		echo json_encode(array(
			'status'	=> 1,
			'message'	=> "example"
		));

		wp_die();
	}
	
	add_action('wp_ajax_ajax_example', 'ajax_example');
	add_action('wp_ajax_nopriv_ajax_example', 'ajax_example');