<?php
	// add REST Endpoint
	add_action( 'rest_api_init', function() {
		register_rest_route( 'rest/v1', '/test', array(
			'methods'	=> "GET",
			'callback'	=> function () {
				return array(
					'status'	=> true,
					'message'	=> "Teste de API REST"
				);
			}
		) );
	} );
?>