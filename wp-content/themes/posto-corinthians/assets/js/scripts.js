// Modal

jQuery(document).ready(function($) 
{	
	//Mobile header menu
	$('#main-header .hamburger').on('click', function(e)
	{
		$('#main-header .mobile-menu').toggleClass('opened');
		$('#main-header .hamburger').toggleClass('opened');
		$('#main-header .logo').toggleClass('opened');
	});

	$('#main-menu-close').hide();
	
	$('#main-header .hamburger-box').on('click', function(e){
		$('.hamburger-box').toggle();
		$('#main-menu-close').toggle();
	});

	$('#main-header #main-menu-close').on('click', function(e){
		$('.hamburger-box').toggle();
		$('#main-menu-close').toggle();
	});

	// Lógica Modal
	var bgDark = document.getElementsByClassName("dark-bg")[0];
	var partialResultModal = document.getElementById("partial-result");
	var aboutActionModal = document.getElementById("about-action");
	var votingModal = document.getElementById("voting");
	var btnPartialResult = document.getElementById("menu-partial-result");
	var btnAboutAction = document.getElementById("menu-about-action");
	var btnVote = document.querySelectorAll(".btn-vote");
    var btnVoteSingle = document.getElementsByClassName("btn-vote-single")[0];
    var btnVoteSingleMobile = document.getElementsByClassName("btn-vote-single-mobile")[0];
	var btnShowResult = document.getElementById("show-parcial-result");
	var btnClose = document.getElementById("btn-close");
	var btnClose2 = document.getElementById("btn-close2");
	var btnClose3 = document.getElementById("btn-close3");

	btnPartialResult.onclick = function(){
		bgDark.style.display = "block";
		partialResultModal.style.display = "block";
		jQuery('body').css('overflow', 'hidden');
	}

	btnAboutAction.onclick = function(){
		bgDark.style.display = "block";
		aboutActionModal.style.display = "block";
		jQuery('body').css('overflow', 'hidden');
	}

    if(btnVote.length>0){
        btnVote.forEach(function(button) {
            button.onclick = function(){
                votingModal.getElementsByTagName("h4")[0].innerText = jQuery(this).parent().parent().parent().parent().find("h3").text();
                votingModal.getElementsByTagName("img")[0].src = jQuery(this).parent().parent().parent().parent().find("img").attr("src");
                // votingModal.getElementsByTagName("data-id")[0].src = pathing.attr("data-id");
                bgDark.style.display = "block";
                votingModal.style.display = "block";
                jQuery('body').css('overflow', 'hidden');
            }
        });
    }

    if(btnVoteSingle!=undefined){
        btnVoteSingle.onclick = function(){
		    votingModal.getElementsByTagName("h4")[0].innerText = jQuery(this).parent().parent().find("h1").text();
			votingModal.getElementsByTagName("img")[0].src = jQuery(this).parent().parent().parent().parent().parent().find(".image").attr("src")
            // votingModal.getElementsByTagName("data-id")[0].attr("data-id");
			bgDark.style.display = "block";
			votingModal.style.display = "block";
			jQuery('body').css('overflow', 'hidden');
	    }

        btnVoteSingleMobile.onclick = function(){
            votingModal.getElementsByTagName("h4")[0].innerText = jQuery(this).parent().parent().parent().parent().parent().parent().find(".author-title").text()
            votingModal.getElementsByTagName("img")[0].src = jQuery(this).parent().parent().parent().parent().parent().find(".image").attr("src")
            // votingModal.getElementsByTagName("data-id")[0].src = pathing.attr("data-id");
            bgDark.style.display = "block";
            votingModal.style.display = "block";
            jQuery('body').css('overflow', 'hidden');
        }
    }
  
	btnClose.onclick = function() {
        bgDark.style.display = "none";
		partialResultModal.style.display = "none";
		aboutActionModal.style.display = "none";
		votingModal.style.display = "none";
		jQuery('body').css('overflow', 'auto');
	}

	btnClose2.onclick = function() {
        bgDark.style.display = "none";
		partialResultModal.style.display = "none";
		aboutActionModal.style.display = "none";
		votingModal.style.display = "none";
		jQuery('body').css('overflow', 'auto');
	}

	btnClose3.onclick = function() {
        bgDark.style.display = "none";
		partialResultModal.style.display = "none";
		aboutActionModal.style.display = "none";
		votingModal.style.display = "none";
		jQuery('body').css('overflow', 'auto');
	}

	bgDark.onclick = function(event) {
		bgDark.style.display = "none";
		partialResultModal.style.display = "none";
		aboutActionModal.style.display = "none";
		votingModal.style.display = "none";
		jQuery('body').css('overflow', 'auto');
	}

    // function onClick(e) {
    //     e.preventDefault();
    //     grecaptcha.ready(function() {
    //       grecaptcha.execute('6Leg7IYcAAAAAMyOxgY5h4ALfh_D35Of2P0fKrWY', {action: 'submit'}).then(function(token) {
    //           // Add your logic to submit to your backend server here.
    //       });
    //     });
    // }
});