jQuery(document).ready(function($) 
{	
    $('.slickCarrousel').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav'
        
    });

    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slickCarrousel',
        infinite: true,
        centerMode: true,
        focusOnSelect: true
    });
});