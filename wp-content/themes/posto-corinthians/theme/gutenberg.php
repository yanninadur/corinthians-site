<?php
	/*******************************
	GUTENBERG BLOCKS
	********************************/
	add_action('acf/init', function() {
		
		// check function exists
		if( function_exists('acf_register_block') ) {
			
			// register custom blocks
			acf_register_block( array(
				'name'				=> 'example',
				'title'				=> __('Bloco de exemplo'),
				'description'		=> __('Um bloco para exemplificar.'),
				'render_callback'	=> 'acf_block_render_callback',
				'category'			=> 'widgets',
				'icon'				=> 'carrot',
				'keywords'			=> array( 'example' )
			));
		}
	});

	function acf_block_render_callback( $block ) {
		$slug = str_replace('acf/', '', $block['name']);
		
		// include a template
		if( file_exists( get_theme_file_path("blocks/{$slug}.php") ) ) {
			include( get_theme_file_path("blocks/{$slug}.php") );
		}
	}

	add_filter( 'allowed_block_types', function( $allowed_blocks ) {
		return array(
			'core/heading',
			'core/paragraph',
			'core/image',
			'core/list',
			'core/gallery',
			'core/quote',
			'core/table',
			'core/code',
			'core/html',
			'core/block',
			'core/freeform',
			'core/shortcode',
			'core-embed/youtube',
			'core-embed/twitter',
			'core-embed/facebook',
			'core-embed/instagram',
			
			'acf/example',
		);
	} );