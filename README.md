# WP Boilerplate

    Boilerplate de temas WP

## Estrutura de pastas do tema
> Tudo é referente a pasta do tema
```
wp-content/themes/theme
├── assets
    └── * Código fonte *
	└── fonts   - Fontes
	└── images  - Imagens
	└── jade    - Templates em jade
	└── js      - scripts
	└── sass    - Estilos em .scss
    └── json    - Arquivos em json utilizados
├── library
	└── * Arquivos gerados automaticamente *
└── src
	└── * Arquivos de configurações *
```
## Dependências
O projeto depende:
- [node.js](https://nodejs.org)
- [sass](https://sass-lang.com)
- [gulp](https://gulpjs.com)

## Instalando dependências do node
O `package.js` encontra-se na pasta `src`
> `$ cd src/`

Instalar as dependências do node
> `$ npm install`

### comandos gulp
Os comandos devem ser utilizados a partir da pasta `src`

**padrão:** `$ gulp` - Compilar todos os arquivos
 
**build:** `$ gulp build` - Compilar todos os arquivos
 
**release:** `$ gulp build --release` - Compilar todos os arquivos minificando *js*, *css* e *imagens*

> A opção `--release` pode ser usada em qualquer um dos comandos `gulp`
 
**watch:** `$ gulp watch` - Compilar automaticamente quando salvar os arquivos
> Rodar o comando novamente ao adicionar novos arquivos Sass

#### Outros comandos gulp
**jade:** `$ gulp jade` - Compilar o jade

**images:** `$ gulp images` - colocar as imagens originais na pasta `library/images`

**styles:** `$ gulp styles` - compilar o sass


**js:** `$ gulp js`
> colocar as bibliotecas em js para a pasta `library/js/libs`

**fonts:** `$ gulp fonts`
> colocar as as fonts na pasta `library/fonts`

## Produção
As pastas
```
├── assets
└── src
```
**NÃO** devem ser colocadas no site em produção