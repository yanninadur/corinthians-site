<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'posto_corinthians' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'teste123' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1cOZ8?1_FXIP!IHluGUxC8ReyfF6q3!rU@6NJIn/jP{xxMfluN1D@_N,36x_-Go1' );
define( 'SECURE_AUTH_KEY',  'af52O7hcFHbG^?t]-j[n.>[pEW1RKA<f6BH-4kjDc<>Gp_$l5r&t[fVO=:#h02d{' );
define( 'LOGGED_IN_KEY',    '3~WO`Y]ddyY]63<(d3lMZ4a_<<4o$#M6C|r9=?_gKDf@] U}>52UiitGK?Z*6_Nx' );
define( 'NONCE_KEY',        'cSjW[=V90S6De[e2}rK gV26L7gEvTgfH]_7cM9jv2Z(cEgsj6nH4eep~|]PBmb)' );
define( 'AUTH_SALT',        '; 9G#ni~SI:7n@AAUBG0LhXOI9H.B(6Y4d<@Sttb}6Ons46TLq|t_F-(^toYlEtH' );
define( 'SECURE_AUTH_SALT', '%x W/A}qzT>]m*<pj~<+,g_LVUUsd}n9*$C_6w~d2kb%FYMmv(p;3cviaq#,u2 #' );
define( 'LOGGED_IN_SALT',   'QAmA)011b<xo[IC!Es|1/W,n.dz4a-~VB3 B4GLCo9DIH[k~#6@d|r,aDJF7I(R>' );
define( 'NONCE_SALT',       '=xBUWHlO8%1vE!Ya4jZk-gG357)Q^waYUEWF!m?BL5&RB|>9$VY>.k3YH-<Fkzyz' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define('FS_METHOD', 'direct');


/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
